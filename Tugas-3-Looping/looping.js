//SOAL NO.1
console.log('SOAL NO.1');
console.log('LOOPING PERTAMA');
var a = 1;
while (a <= 20) {
  if (a % 2 === 0)
  console.log(a +' - I love coding');
  a++;
}

console.log();
console.log('LOOPING KEDUA');
var b = 20;
while (b <= 20 && b > 0) {
  if (b % 2 === 0)
  console.log(b +' - I will become a mobile developer');
  b--;
}

//SOAL NO.2

console.log();
console.log('SOAL NO.2');
for (i=1; i <= 20; i++)
{
  if (i % 2 != 0)
    {
      if (i % 3 === 0)
      {
        console.log(i+' - I Love Coding')
      }
      else
      {
         console.log(i+ ' - Santai')
      }
    }
  else
      if (i % 3 === 0)
      {
        console.log(i+' - I Love Coding')
      }
      else
      {
         console.log(i+ ' - Berkualitas')
      }
}

//SOAL NO.3

console.log();
console.log('SOAL NO.3');

var i = 1;
while (i < 5)
{
console.log('########')
i++
}

//SOAL NO.4

console.log();
console.log('SOAL NO.4');
var i = 1;
while (i <= 7)
{
  if (i == 1)
      {
        console.log('#')
      }
  if (i == 2)
      {
        console.log('##')
      }
  if (i == 3)
      {
        console.log('###')
      }
  if (i == 4)
      {
        console.log('####')
      }
  if (i == 5)
      {
        console.log('#####')
      }
  if (i == 6)
      {
        console.log('######')
      }
  if (i == 7)
      {
        console.log('#######')
      }
i++
}

//SOAL NO.5


console.log();
console.log('SOAL NO.5');
var i = 1;
while (i <= 8)
{
  if (i % 2 != 0)
  {
    console.log(' # # # #')
  }
  else
  {
    console.log('# # # #')
  }
i++
}
